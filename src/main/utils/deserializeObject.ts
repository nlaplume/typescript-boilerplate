import ISerializable from 'interfaces/ISerializable'

interface IClass<T extends ISerializable> {
	new (): T;
}

function deserializeObject<T extends ISerializable>(
	input: any, Class: IClass<T>
	){
		
	var o = new Class();
	o.deserialize(input);
	return o;
}

export default deserializeObject;