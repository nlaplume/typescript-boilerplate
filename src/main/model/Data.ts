import ISerializable from 'interfaces/ISerializable'
import Utils from 'utils/Utils'

export default class Data implements ISerializable {
	
	private static saveName = 'save';
	
	
	constructor() {	
		this.load();
	}

	save(){
		localStorage.setItem(Data.saveName, JSON.stringify(this));
	}
	
	load(){
		this.deserialize(JSON.parse(localStorage.getItem(Data.saveName)));
	}

	deserialize(input: Data):Data{
		//this.array = Utils.deserializeArray(input.array, Type);
		//this.object = Utils.deserializeObject(input.object, Type);
		//this.primitive = input.primitive;
		return this;
	}
}