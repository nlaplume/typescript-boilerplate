import Data from 'model/Data'
import MainController from 'controllers/MainController'

export default class BaseController{

	public static data = new Data();
	protected static mainController: MainController;

	constructor($routeParams?: angular.route.IRouteParamsService){
	}
	
	data () {
		return BaseController.data;
	}
	
	protected goTo(page: string){
		BaseController.mainController.setCurrentView(page);
	}
	
	
	
}
