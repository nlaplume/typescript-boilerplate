import HomeController from 'controllers/HomeController';
import MainController from 'controllers/MainController';



export default class AppMain{
	public static Run(angular: angular.IAngularStatic){
		new AppMain();
	}
	
	private appModule : ng.IModule;
	
	public constructor(){
		
		this.appModule = angular.module('MyApp', ['ngRoute', 'ui.bootstrap', 'pascalprecht.translate']);
		
		this.setRoutes();
		this.setControllers();
		
		angular.element(document).ready(()=>{
			angular.bootstrap(document, ['MyApp']);
		});
		
	}
	
	private setRoutes(){
		this.appModule.config(['$routeProvider', '$translateProvider', 
			function($routeProvider: angular.route.IRouteProvider, 
					 $translateProvider: angular.translate.ITranslateProvider)
			{
			$routeProvider.when('/', {
				templateUrl: '/src/pages/home.html',
				controller: 'HomeController'
			}).otherwise({
				templateUrl: '/src/pages/home.html',
				controller: 'HomeController'
			});
			
			$translateProvider.useStaticFilesLoader({
				prefix: '/src/translations/',
				suffix: '.json'
			});
			$translateProvider.preferredLanguage('en');
			$translateProvider.useSanitizeValueStrategy('escape');
			
			//$locationProvider.html5Mode(true);
		}]);
	}
	
	private setControllers(){
		this.appModule.controller('MainController', ($scope: angular.IScope, $location)=>{
			$scope['MainController'] = new MainController($location);
		});
		this.appModule.controller('HomeController', ($scope: angular.IScope)=>{
			$scope['Ctrl'] = new HomeController();
		});
		/*
		this.appModule.controller('GreenlightController', ($scope: angular.IScope, $routeParams: angular.route.IRouteParamsService)=>{
			$scope['Ctrl'] = new GreenlightController($routeParams);
		});
		*/

	}
}