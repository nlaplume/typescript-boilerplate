'use strict';

module.exports = function(grunt) {
  
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    shell: {
        'http-server': {
          command: 'http-server -p9000 -c-1 -o -s'
        },
        
        'typescript': {
          command: 'tsc --project .'
        },
        
        'typescript-watch': {
          command: 'tsc --project . --watch'
        },
        
        'library' : {
          command: function(lib){
            var bower = 'bower install ' + lib + ' --save',
              tsd = 'tsd install ' + lib + ' --save';
              return [bower, tsd].join(' && ');
          }
        }
    },
    
    clean: ['target'],
    
    
    concurrent: {
      options:{
        logConcurrentOutput: true  
      },
      start: ['shell:typescript-watch', 'shell:http-server']
    },
    
    
      
    
    
  });
  
  grunt.registerTask('default', ['concurrent:start']);  
  grunt.registerTask('compile', ['clean', 'shell:typescript']);



};


