var lineRegex = /%([a-zA-Z]*)%/g;

export default {
	nextBool(chances = 50){
		return Math.random()*100 > chances;
	},
	nextNumber(min: number, max: number){
		return Math.random() * (max-min) + min;
	},
	nextElement(array: Array<any>){
		return array[this.nextInt(0, array.length)];
	},
	nextStd(avg: number, stdDev: number){
		var u = Math.random(), 
			v = Math.random(),
			z = Math.sqrt(-2 * Math.log(u)) * Math.cos(Math.PI * 2 * v);
		return (z * stdDev + avg);
	},

	nextEnum(type: any, min: number, max=0){
		if (max ===0){
			max = min;
			min = 0;
		}
		return type[type[this.nextInt(min, max)]];
	},
	
	
	nextInt(min:number, max:number){
		return Math.floor(this.nextNumber(min, max));
	},
	nextStdInt(avg: number, stdDev: number){
		return Math.round(this.nextStd(avg, stdDev));
	},
	
	parse(line: string, data){
		return line.replace(lineRegex, (all, match) => {return this.nextElement(data[match])});
	}
};