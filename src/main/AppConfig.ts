require.config({
	baseUrl: '/target/main',
	paths: {
		'jquery': '../../bower_components/jquery/dist/jquery',
		'underscore': '../../bower_components/underscore/underscore',
		'angular' : '../../bower_components/angular/angular',
		'angular-route' : '../../bower_components/angular-route/angular-route',
		'angular-ui': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
		'angular-translate': '../../bower_components/angular-translate/angular-translate',
		'angular-translate-static': '../../bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files'
	},
	
	shim: {
		underscore:{
			exports: "_"
		},
		angular: {
			exports: 'angular'
		},
		'angular-ui': {
			deps: ['angular']
		},
		'angular-route': {
			exports: 'ngRoute',
			deps: ['angular']
		},
		'angular-translate': {
			deps: ['angular']
		} ,
		'angular-translate-static': {
			deps: ['angular-translate']
		} 
	}
});


require(['AppMain', 'angular', 'underscore', 'angular-ui', 'angular-route', 'angular-translate', 'angular-translate-static'
], (AppMain, angular) => {
	AppMain.default.Run(angular);	
});
