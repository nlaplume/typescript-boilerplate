# Typescript Boilerplate #

This is a project to learn some typescript and to have an automated build 

### How do I get set up? ###

* npm install
* npm install -g grunt
* npm install -g bower
* npm install -g tsd
* npm install -g tsc
* npm install -g http-server
* bower install 
* tsd reinstall
* grunt

* Don't forgot to add a .bowerrc if you are behind a proxy
