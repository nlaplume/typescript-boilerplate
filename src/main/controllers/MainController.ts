import BaseController from 'controllers/BaseController'

export default class MainController extends BaseController{
	
	currentView = 'home';
	$location: angular.ILocationService;
	
	constructor($location: angular.ILocationService, $routeParams?: angular.route.IRouteParamsService){
		super($routeParams);
		BaseController.mainController = this;
		this.$location = $location;
	}
	
	getCurrentView(){
		return 'src/pages/' + this.currentView + '.html';
	}
	
	setCurrentView(page: string){
		this.$location.path(page);
	}
}
