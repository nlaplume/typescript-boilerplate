import deserializeObject from 'utils/deserializeObject'
import deserializeArray from 'utils/deserializeArray'

export default {
	deserializeObject: deserializeObject,
	deserializeArray: deserializeArray
};