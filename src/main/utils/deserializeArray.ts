import ISerializable from 'interfaces/ISerializable'
import deserializeObject from 'utils/deserializeObject'

interface IClass<T extends ISerializable> {
	new (): T;
}

function deserializeArray<T extends ISerializable>(
	input: Array<any>, Class: IClass<T>
	){
		
	var array :Array<T> = [];
	_.each(input, (value: any)=>{
		array.push(deserializeObject(value, Class));
	});
	return array;
}

export default deserializeArray;