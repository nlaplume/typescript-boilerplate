import BaseController from 'controllers/BaseController'

export default class HomeController extends BaseController{
	
	constructor($routeParams?: angular.route.IRouteParamsService){
		super($routeParams);
	}
	
}
