interface ISerializable {
	deserialize(input: any): void;
}

export default ISerializable;